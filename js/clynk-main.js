$( function() {

    // Activating Sticky navbar on Scroll 
    $(window).scroll(function() {
        var height = $(window).scrollTop();

        if(height  > 30) {
            $('.top-header').addClass('sticky');
        }

        if(height < 30){
            $('.top-header').removeClass('sticky');
        }
    });

    $(".searchBar").on("click",function(e){
        $(".search-wrapper-top").addClass("show");
    })

    $(".search-wrapper-top .close-btn").on("click",function(e){
        $(".search-wrapper-top").removeClass("show");
    })


    /** Opening the User Menu  */
    var menu = $(".user-menu");
    $(".user-menu-btn").on("click",function(e){
        if($(".user-menu-open").length > 0){
            $(menu).removeClass("user-menu-open");
            return;
        }
        $(menu).addClass("user-menu-open");
    });


    /** DropDown Main Nav Menu */
    $(document).on("click",".open_menu",function(e){
        e.preventDefault();
        $(this).html("<i class='fas fa-times'></i>");
        $(this).addClass("close_menu");
        $(this).removeClass("open_menu");
        $(".mobile-menu-wrapper").addClass("opened");
    })


    $(document).on("click",".close_menu",function(e){
        $(this).html(`<i class="fas fa-equals"></i>`);
        $(".mobile-menu-wrapper").removeClass("opened");
        $(this).removeClass("close_menu");
        $(this).addClass("open_menu");
    })
    

    /** Modal || Custom Tab Handling with Transitions */
    $("[data-shift-tabs]").on("click",function(e){
        e.preventDefault();
        var tab = $(this).data("shift-tabs");
        var activeTab = $(".activeTab");
        if(activeTab.length > 0){
            $(".activeTab").removeClass("activeTab");
        }
        $(`.${tab}`).addClass("activeTab");
    });
    

    /** Reset Modal when Modal is hidden 
     * This will only happen if user doesnt complete second step and closes modal. 
     * Else, User will be taken forward.
    */
    var signUpModal = document.getElementById("signUpModal");
    signUpModal.addEventListener('hidden.bs.modal', function (event) {
        tabReset();
    })

    /** Sliders and Carousels */
    
    /** Where to Cards */
    $(".where-cards").slick({
        slidesToShow:4,
        slidesToScroll:1,
        arrows:false,
        responsive:[
            {
            breakpoint: 576,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
            },
        ]
    })
    
    // ---------------------------------- Explore Cards ------------------------------
    const exploreCardsSettings = {
        slidesToShow:4,
        slidesToScroll:1,
        arrows:true,
        prevArrow:"<button class='slick-prev'><i class='fas fa-angle-left'></i></button>",
        nextArrow:"<button class='slick-next'><i class='fas fa-angle-right'></i></button>",
        responsive:[
            {
            breakpoint: 576,
            settings: {
                arrows:false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
            },
        ]
    }
    $(".explore-cards").slick(exploreCardsSettings)
    window.addEventListener('resize', function () {
        if(window.innerWidth < 992){
            $(".explore-cards").slick("unslick");
        }else{
            $(".explore-cards").slick(exploreCardsSettings)
        }
    })
    if(window.innerWidth < 992){
        $(".explore-cards").slick("unslick");
    }
    
    // ---------------------------------- End Explore Cards ---------------------------
    
    // Explore Cards
    const vipCardsSettings = {
        slidesToShow:4,
        slidesToScroll:1,
        arrows:false,
        prevArrow:"<button class='slick-prev'><i class='fas fa-angle-left'></i></button>",
        nextArrow:"<button class='slick-next'><i class='fas fa-angle-right'></i></button>",
        responsive:[
            {
            breakpoint: 576,
            settings: {
                arrows:false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
            },
            {
            breakpoint: 1024,
            settings: "unslick"
            },
        ]
        
    }
    $(".vip-cards").slick(vipCardsSettings)
    // Unslick on desktop
    window.addEventListener('resize', function () {
        if(window.innerWidth > 768){
            $(".vip-cards").slick("unslick");
        }else{
            $(".vip-cards").slick(vipCardsSettings);
        }
    })
    if(window.innerWidth > 768){
        $(".vip-cards").slick("unslick");
    }
    
    
    /* Testimonials Slider */
    $(".testimonials").slick({
        slidesToShow:1,
        slidesToScroll:1,
        arrows:true,
        infinite: true,
        prevArrow:"<button class='slick-prev'><i class='fas fa-angle-left'></i></button>",
        nextArrow:"<button class='slick-next'><i class='fas fa-angle-right'></i></button>",
        responsive:[
            {
                breakpoint: 576,
                settings: {
                    arrows:false,
                    slidesToShow: 1,
                    slidesToScroll: 1,                    
                    dots: true
                }
            },
        ]
    })
    
    // Great Events
    const greatEventSettings = {
        slidesToShow:4,
        slidesToScroll:1,
        arrows:false,
        prevArrow:"<button class='slick-prev'><i class='fas fa-angle-left'></i></button>",
        nextArrow:"<button class='slick-next'><i class='fas fa-angle-right'></i></button>",
        responsive:[
            {
                breakpoint: 576,
                settings: {
                    arrows:false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
        ]
        
    }
    $(".great-events").slick(greatEventSettings)
    // Unslick if on desktop
    window.addEventListener('resize', function () {
        if(window.innerWidth > 768){
            $(".great-events").slick("unslick");
        }else{
            $(".great-events").slick(greatEventSettings)
        }
    })
    if(window.innerWidth > 768){
        $(".great-events").slick("unslick");
    }




    $("#location").on("click",function(e){
        var locationModal = document.getElementById("locationModal");
        $(locationModal).modal("show")
        
    })

    $(".choose_location").on("click",function(e){
        var locationModal = document.getElementById("locationModal");
        $(locationModal).modal("show")
    })
    
    /**
     * Reset Tab
     * triggers when modal closed. 
     */
    function tabReset(){
        var getCurrentTab = $(".activeTab");
        $(".activeTab").removeClass("activeTab");
        $(".step-1").addClass("activeTab");
    }
    
    /** Show More Login Options */
    $("#login_options").on("click",function(e){
        $("#login-options-social").toggle(300);
    })

    
    $(document).on("click",".preset-locations li",function(e){
        $(".preset-locations li.active").removeClass("active");
        $(this).addClass("active");
    })

    
    /** Date Picker w/ Future Date > Today Date Validation */
    var dateFormat = "mm/dd/yy",
    from = $( ".date-pick-from" )
    .datepicker({
        changeMonth: true,
        numberOfMonths: 1
    })
    .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
    }),
    to = $( ".date-pick-to" ).datepicker({
        changeMonth: true,
        numberOfMonths: 1
    })
    .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
    });

    function getDate( element ) {
        var date;
        try {
        date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
        date = null;
        }
        return date;
    }


    
    
  } );